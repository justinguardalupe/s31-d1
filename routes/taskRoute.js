// Setup express dependency
const express = require("express");
// Create a Router instance
const router = express.Router();
// import TaskController.js
const taskController = require("../controllers/taskController");

// Routes

// Route to get all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a new task
router.get("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// route for deleting a task
// localhost: 3001/tasks/09876b098798y0
router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// route for updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(`Task updated: ${resultFromController}`));
})

// Activity

// #1 Route for getting a specific task
router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(`Task found: ${resultFromController}`));
})

// #4 Route for changing task status
router.put("/:id/complete", (req, res) => {
taskController.completeTask(req.params.id, req.body).then(resultFromController => res.send(`Task completed: ${resultFromController}`));
})


module.exports = router;
